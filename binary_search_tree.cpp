#include <iostream>
using namespace std;

template <class T, class U>
class BinarySearchTree {

	struct BinaryTreeNode {
		public:
			T key;
			U value;
			BinaryTreeNode *left;
			BinaryTreeNode *right;
	};

	typedef struct BinaryTreeNode BinaryTreeNode;

	public:
		int getSize();
		BinaryTreeNode getRoot();
		void insert(T new_key, U new_elem);
		void printInOrder();
		bool searchElem(T search_key);
		void deleteElem(T del_key);

		BinarySearchTree(void) {
			root = NULL;
		}

		~BinarySearchTree(void) {
			deleteAll(root);
		}

	private:
		BinaryTreeNode *root;
		int size;

		void printInOrderHelper(BinaryTreeNode *root) {
			if (root == NULL) return;
			else {
				printInOrderHelper(root->left);
				cout << root->key;
				cout << " - " << root->value << endl;
				printInOrderHelper(root->right);
			}
		};

		void deleteElemHelper(BinaryTreeNode *root, T del_elem) {
			BinaryTreeNode *temp = root;
			BinaryTreeNode *t = NULL;
			int dir = -1;

			while(1) {
				if (temp == NULL) {
					return;
				}

				else if (temp->key > del_elem) {
					t = temp;
					dir = 0;
					temp = temp->left;
				}

				else if (temp->key < del_elem) {
					t = temp;
					dir = 1;
					temp = temp->right;
				}

				// leaf node
				else if (temp->key == del_elem
					&& temp->left == NULL
					&& temp->right == NULL
				) {
					if (dir == 0) t->left = NULL;
					else if (dir == 1) t->right = NULL;
					delete temp;
					return;
				}

				else if (temp->key == del_elem
					&& temp->left == NULL
				) {
					if (dir == 0) t->left = temp->right;
					else if (dir == 1) t->right = temp->right;
					delete temp;
					return;
				}

				else if (temp->key == del_elem
					&& temp->right == NULL
				) {
					if (dir == 0) t->left = temp->left;
					else if (dir == 1) t->right = temp->left;
					delete temp;
					return;
				}

				else if (temp->key == del_elem
					&& temp->left && temp->right
				) {
					BinaryTreeNode* min = findMin(temp->right);
					T stored = min->key;
					deleteElemHelper(root, min->key);
					temp->key = stored;
					return;
				}
			}
		};

		BinaryTreeNode* findMin(BinaryTreeNode *node) {
			BinaryTreeNode* curr = node;

			while (curr->left != NULL)
			    curr = curr->left;

			return curr;
		}
};


template <class T, class U>
int BinarySearchTree<T, U>::getSize() {
	return size;
};

template <class T, class U>
void BinarySearchTree<T, U>::insert(T new_key, U new_elem) {
	BinaryTreeNode *temp = root;
	BinaryTreeNode *newNode = new BinaryTreeNode();
	newNode->key = new_key;
	newNode->value = new_elem;
	newNode->left = NULL; newNode->right = NULL;
	size++;

	if (root == NULL) {
		root = newNode;
		return;
	}

	while(1) {
		if (temp == NULL) {
			break;
		}

		else if (temp->key > new_key) {
			if (temp->left == NULL) {
				temp->left = newNode;
				break;
			} else {
				temp = temp->left;
			}
		}

		else if (temp->key < new_key) {
			if (temp->right == NULL) {
				temp->right = newNode;
				break;
			} else {
				temp = temp->right;
			}
		}
	}
};

template <class T, class U>
void BinarySearchTree<T, U>::printInOrder() {
	printInOrderHelper(root);
};

template <class T, class U>
bool BinarySearchTree<T, U>::searchElem(T search_key) {
	BinaryTreeNode *temp = root;

	if (root == NULL) {
		return false;
	}

	while(1) {
		if (temp == NULL) {
			return false;
		}

		else if (temp->key > search_key) {
			temp = temp->left;
		}

		else if (temp->key < search_key) {
			temp = temp->right;
		}

		else {
			return true;
		}
	}
};

template <class T, class U>
void BinarySearchTree<T, U>::deleteElem(T del_elem) {
	if (root == NULL) return;

	else if (! searchElem(del_elem)) {
		return;
	}
	else {
		deleteElemHelper(root, del_elem);
	}
};



int main() {

	BinarySearchTree<int, string> *b = new BinarySearchTree<int, string>();
	b->insert(50, "A");
	b->insert(30, "B");
	b->insert(20, "C");
	b->insert(40, "D");
	b->insert(70, "E");
	b->insert(60, "F");
	b->insert(80, "G");
	b->printInOrder();


	b->deleteElem(20);
	cout << "\n";
	b->printInOrder();


	b->deleteElem(30);
	cout << "\n";
	b->printInOrder();


	b->deleteElem(50);
	cout << "\n";
	b->printInOrder();
}
